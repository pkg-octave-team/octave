<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Matrices and Arrays in Oct-Files (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Matrices and Arrays in Oct-Files (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Matrices and Arrays in Oct-Files (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Oct_002dFiles.html" rel="up" title="Oct-Files">
<link href="Character-Strings-in-Oct_002dFiles.html" rel="next" title="Character Strings in Oct-Files">
<link href="Getting-Started-with-Oct_002dFiles.html" rel="prev" title="Getting Started with Oct-Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Matrices-and-Arrays-in-Oct_002dFiles">
<div class="nav-panel">
<p>
Next: <a href="Character-Strings-in-Oct_002dFiles.html" accesskey="n" rel="next">Character Strings in Oct-Files</a>, Previous: <a href="Getting-Started-with-Oct_002dFiles.html" accesskey="p" rel="prev">Getting Started with Oct-Files</a>, Up: <a href="Oct_002dFiles.html" accesskey="u" rel="up">Oct-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Matrices-and-Arrays-in-Oct_002dFiles-1"><span>A.1.2 Matrices and Arrays in Oct-Files<a class="copiable-link" href="#Matrices-and-Arrays-in-Oct_002dFiles-1"> &para;</a></span></h4>

<p>Octave supports a number of different array and matrix classes, the majority of
which are based on the <code class="code">Array</code> class.  The exception are the sparse matrix
types discussed separately below.  There are three basic matrix types:
</p>
<dl class="table">
<dt><code class="code">Matrix</code></dt>
<dd><p>A double precision matrix class defined in <samp class="file">dMatrix.h</samp>
</p>
</dd>
<dt><code class="code">ComplexMatrix</code></dt>
<dd><p>A complex matrix class defined in <samp class="file">CMatrix.h</samp>
</p>
</dd>
<dt><code class="code">BoolMatrix</code></dt>
<dd><p>A boolean matrix class defined in <samp class="file">boolMatrix.h</samp>
</p></dd>
</dl>

<p>These are the basic two-dimensional matrix types of Octave.  In addition there
are a number of multi-dimensional array types including
</p>
<dl class="table">
<dt><code class="code">NDArray</code></dt>
<dd><p>A double precision array class defined in <samp class="file">dNDArray.h</samp>
</p>
</dd>
<dt><code class="code">ComplexNDarray</code></dt>
<dd><p>A complex array class defined in <samp class="file">CNDArray.h</samp>
</p>
</dd>
<dt><code class="code">boolNDArray</code></dt>
<dd><p>A boolean array class defined in <samp class="file">boolNDArray.h</samp>
</p>
</dd>
<dt><code class="code">int8NDArray</code></dt>
<dt><code class="code">int16NDArray</code></dt>
<dt><code class="code">int32NDArray</code></dt>
<dt><code class="code">int64NDArray</code></dt>
<dd><p>8, 16, 32, and 64-bit signed array classes defined in
<samp class="file">int8NDArray.h</samp>, <samp class="file">int16NDArray.h</samp>, etc.
</p>
</dd>
<dt><code class="code">uint8NDArray</code></dt>
<dt><code class="code">uint16NDArray</code></dt>
<dt><code class="code">uint32NDArray</code></dt>
<dt><code class="code">uint64NDArray</code></dt>
<dd><p>8, 16, 32, and 64-bit unsigned array classes defined in
<samp class="file">uint8NDArray.h</samp>, <samp class="file">uint16NDArray.h</samp>, etc.
</p></dd>
</dl>

<p>There are several basic ways of constructing matrices or multi-dimensional
arrays.  Using the class <code class="code">Matrix</code> as an example one can
</p>
<ul class="itemize mark-bullet">
<li>Create an empty matrix or array with the empty constructor.  For example:

<div class="example">
<pre class="example-preformatted">Matrix a;
</pre></div>

<p>This can be used for all matrix and array types.
</p>
</li><li>Define the dimensions of the matrix or array with a dim_vector which has the
same characteristics as the vector returned from <code class="code">size</code>.  For example:

<div class="example">
<div class="group"><pre class="example-preformatted">dim_vector dv (2, 3);  // 2 rows, 3 columns
Matrix a (dv);
</pre></div></div>

<p>This can be used for all matrix and array types.
</p>
</li><li>Define the number of rows and columns in the matrix.  For example:

<div class="example">
<pre class="example-preformatted">Matrix a (2, 2)
</pre></div>

<p>This constructor can <strong class="strong">only</strong> be used with matrix types.
</p></li></ul>

<p>These types all share a number of basic methods and operators.  Many bear a
resemblance to functions that exist in the interpreter.  A selection of useful
methods include
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-operator"><span class="category-def">Method: </span><span><code class="def-type">T&amp;</code> <strong class="def-name">operator</strong> <code class="def-code-arguments">() (octave_idx_type)</code><a class="copiable-link" href="#index-operator"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-elem"><span class="category-def">Method: </span><span><code class="def-type">T&amp;</code> <strong class="def-name">elem</strong> <code class="def-code-arguments">(octave_idx_type)</code><a class="copiable-link" href="#index-elem"> &para;</a></span></dt>
<dd><p>The <code class="code">()</code> operator or <code class="code">elem</code> method allow the values of the matrix or
array to be read or set.  These methods take a single argument, which is of
type <code class="code">octave_idx_type</code>, that is the index into the matrix or array.
Additionally, the matrix type allows two argument versions of the <code class="code">()</code>
operator and <code class="code">elem</code> method, giving the row and column index of the value
to get or set.
</p></dd></dl>

<p>Note that these functions do significant error checking and so in some
circumstances the user might prefer to access the data of the array or matrix
directly through the <code class="code">fortran_vec</code> method discussed below.
</p>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-numel-2"><span class="category-def">Method: </span><span><code class="def-type">octave_idx_type</code> <strong class="def-name">numel</strong> <code class="def-code-arguments">() const</code><a class="copiable-link" href="#index-numel-2"> &para;</a></span></dt>
<dd><p>The total number of elements in the matrix or array.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-byte_005fsize"><span class="category-def">Method: </span><span><code class="def-type">size_t</code> <strong class="def-name">byte_size</strong> <code class="def-code-arguments">() const</code><a class="copiable-link" href="#index-byte_005fsize"> &para;</a></span></dt>
<dd><p>The number of bytes used to store the matrix or array.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-dims"><span class="category-def">Method: </span><span><code class="def-type">dim_vector</code> <strong class="def-name">dims</strong> <code class="def-code-arguments">() const</code><a class="copiable-link" href="#index-dims"> &para;</a></span></dt>
<dd><p>The dimensions of the matrix or array in value of type <code class="code">dim_vector</code>.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-ndims-1"><span class="category-def">Method: </span><span><code class="def-type">int</code> <strong class="def-name">ndims</strong> <code class="def-code-arguments">() const</code><a class="copiable-link" href="#index-ndims-1"> &para;</a></span></dt>
<dd><p>The number of dimensions of the matrix or array.  Matrices are always 2-D, but
arrays can be N-dimensional.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-resize-3"><span class="category-def">Method: </span><span><code class="def-type">void</code> <strong class="def-name">resize</strong> <code class="def-code-arguments">(const dim_vector&amp;)</code><a class="copiable-link" href="#index-resize-3"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-resize-4"><span class="category-def">Method: </span><span><code class="def-type">void</code> <strong class="def-name">resize</strong> <code class="def-code-arguments">(nrows, ncols)</code><a class="copiable-link" href="#index-resize-4"> &para;</a></span></dt>
<dd><p>A method taking either an argument of type <code class="code">dim_vector</code>, or, in the case
of a matrix, two arguments of type <code class="code">octave_idx_type</code> defining the number
of rows and columns in the matrix.
</p></dd></dl>

<dl class="first-deftypefn">
<dt class="deftypefn" id="index-fortran_005fvec"><span class="category-def">Method: </span><span><code class="def-type">T *</code> <strong class="def-name">fortran_vec</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-fortran_005fvec"> &para;</a></span></dt>
<dd><p>This method returns a pointer to the underlying data of the matrix or array so
that it can be manipulated directly, either within Octave or by an external
library.
</p></dd></dl>

<p>Operators such as <code class="code">+</code>, <code class="code">-</code>, or <code class="code">*</code> can be used on the majority
of the matrix and array types.  In addition there are a number of methods that
are of interest only for matrices such as <code class="code">transpose</code>, <code class="code">hermitian</code>,
<code class="code">solve</code>, etc.
</p>
<p>The typical way to extract a matrix or array from the input arguments of
<code class="code">DEFUN_DLD</code><!-- /@w -->&nbsp;function is as follows
</p>
<div class="example">
<div class="group"><pre class="verbatim">#include &lt;octave/oct.h&gt;

DEFUN_DLD (addtwomatrices, args, , &quot;Add A to B&quot;)
{
  if (args.length () != 2)
    print_usage ();

  NDArray A = args(0).array_value ();
  NDArray B = args(1).array_value ();

  return octave_value (A + B);
}
</pre></div></div>

<p>To avoid segmentation faults causing Octave to abort, this function explicitly
checks that there are sufficient arguments available before accessing these
arguments.  It then obtains two multi-dimensional arrays of type <code class="code">NDArray</code>
and adds these together.  Note that the <code class="code">array_value</code> method is called
without using the <code class="code">is_matrix_type</code> method.  If an error occurs when
attempting to extract the value, Octave will print a message and throw an
exception.  The reason to prefer this coding structure is that the arguments
might be a type which is not an <code class="code">NDArray</code>, but for which it would make
sense to convert them to one.  The <code class="code">array_value</code> method allows this
conversion to be performed transparently when possible.  If you need to catch
errors like this, and perform some kind of cleanup or other operation, you can
catch the <code class="code">octave_execution_error</code> exception.
</p>
<p><code class="code">A + B</code>, operating on two <code class="code">NDArray</code> objects returns an
<code class="code">NDArray</code>, which is cast to an <code class="code">octave_value</code> on the return from the
function.  An example of the use of this demonstration function is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">addtwomatrices (ones (2, 2), eye (2, 2))
      &rArr;  2  1
          1  2
</pre></div></div>

<p>A list of the basic <code class="code">Matrix</code> and <code class="code">Array</code> types, the methods to
extract these from an <code class="code">octave_value</code>, and the associated header file is
listed below.
</p>
<table class="multitable">
<thead><tr><th width="30%">Type</th><th width="40%">Function</th><th width="30%">Source Code</th></tr></thead>
<tbody><tr><td width="30%"><code class="code">RowVector</code></td><td width="40%"><code class="code">row_vector_value</code></td><td width="30%"><samp class="file">dRowVector.h</samp></td></tr>
<tr><td width="30%"><code class="code">ComplexRowVector</code></td><td width="40%"><code class="code">complex_row_vector_value</code></td><td width="30%"><samp class="file">CRowVector.h</samp></td></tr>
<tr><td width="30%"><code class="code">ColumnVector</code></td><td width="40%"><code class="code">column_vector_value</code></td><td width="30%"><samp class="file">dColVector.h</samp></td></tr>
<tr><td width="30%"><code class="code">ComplexColumnVector</code></td><td width="40%"><code class="code">complex_column_vector_value</code></td><td width="30%"><samp class="file">CColVector.h</samp></td></tr>
<tr><td width="30%"><code class="code">Matrix</code></td><td width="40%"><code class="code">matrix_value</code></td><td width="30%"><samp class="file">dMatrix.h</samp></td></tr>
<tr><td width="30%"><code class="code">ComplexMatrix</code></td><td width="40%"><code class="code">complex_matrix_value</code></td><td width="30%"><samp class="file">CMatrix.h</samp></td></tr>
<tr><td width="30%"><code class="code">boolMatrix</code></td><td width="40%"><code class="code">bool_matrix_value</code></td><td width="30%"><samp class="file">boolMatrix.h</samp></td></tr>
<tr><td width="30%"><code class="code">charMatrix</code></td><td width="40%"><code class="code">char_matrix_value</code></td><td width="30%"><samp class="file">chMatrix.h</samp></td></tr>
<tr><td width="30%"><code class="code">NDArray</code></td><td width="40%"><code class="code">array_value</code></td><td width="30%"><samp class="file">dNDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">ComplexNDArray</code></td><td width="40%"><code class="code">complex_array_value</code></td><td width="30%"><samp class="file">CNDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">boolNDArray</code></td><td width="40%"><code class="code">bool_array_value</code></td><td width="30%"><samp class="file">boolNDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">charNDArray</code></td><td width="40%"><code class="code">char_array_value</code></td><td width="30%"><samp class="file">charNDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">int8NDArray</code></td><td width="40%"><code class="code">int8_array_value</code></td><td width="30%"><samp class="file">int8NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">int16NDArray</code></td><td width="40%"><code class="code">int16_array_value</code></td><td width="30%"><samp class="file">int16NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">int32NDArray</code></td><td width="40%"><code class="code">int32_array_value</code></td><td width="30%"><samp class="file">int32NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">int64NDArray</code></td><td width="40%"><code class="code">int64_array_value</code></td><td width="30%"><samp class="file">int64NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">uint8NDArray</code></td><td width="40%"><code class="code">uint8_array_value</code></td><td width="30%"><samp class="file">uint8NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">uint16NDArray</code></td><td width="40%"><code class="code">uint16_array_value</code></td><td width="30%"><samp class="file">uint16NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">uint32NDArray</code></td><td width="40%"><code class="code">uint32_array_value</code></td><td width="30%"><samp class="file">uint32NDArray.h</samp></td></tr>
<tr><td width="30%"><code class="code">uint64NDArray</code></td><td width="40%"><code class="code">uint64_array_value</code></td><td width="30%"><samp class="file">uint64NDArray.h</samp></td></tr>
</tbody>
</table>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Character-Strings-in-Oct_002dFiles.html">Character Strings in Oct-Files</a>, Previous: <a href="Getting-Started-with-Oct_002dFiles.html">Getting Started with Oct-Files</a>, Up: <a href="Oct_002dFiles.html">Oct-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
