<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Creating Structures (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Creating Structures (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Creating Structures (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Structures.html" rel="up" title="Structures">
<link href="Manipulating-Structures.html" rel="next" title="Manipulating Structures">
<link href="Structure-Arrays.html" rel="prev" title="Structure Arrays">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Creating-Structures">
<div class="nav-panel">
<p>
Next: <a href="Manipulating-Structures.html" accesskey="n" rel="next">Manipulating Structures</a>, Previous: <a href="Structure-Arrays.html" accesskey="p" rel="prev">Structure Arrays</a>, Up: <a href="Structures.html" accesskey="u" rel="up">Structures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Creating-Structures-1"><span>6.1.3 Creating Structures<a class="copiable-link" href="#Creating-Structures-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-dynamic-naming"></a>

<p>Besides the index operator <code class="code">&quot;.&quot;</code>, Octave can use dynamic naming
<code class="code">&quot;(var)&quot;</code> or the <code class="code">struct</code> function to create structures.  Dynamic
naming uses the string value of a variable as the field name.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = &quot;field2&quot;;
x.a = 1;
x.(a) = 2;
x
     &rArr; x =
        {
          a =  1
          field2 =  2
        }
</pre></div></div>

<p>Dynamic indexing also allows you to use arbitrary strings, not merely
valid Octave identifiers (note that this does not work on <small class="sc">MATLAB</small>):
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = &quot;long field with spaces (and funny char$)&quot;;
x.a = 1;
x.(a) = 2;
x
     &rArr; x =
        {
          a =  1
          long field with spaces (and funny char$) =  2
        }
</pre></div></div>

<p>The warning id <code class="code">Octave:language-extension</code> can be enabled to warn
about this usage.  See <a class="xref" href="Issuing-Warnings.html#XREFwarning_005fids">warning_ids</a>.
</p>
<p>More realistically, all of the functions that operate on strings can be used
to build the correct field name before it is entered into the data structure.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">names = [&quot;Bill&quot;; &quot;Mary&quot;; &quot;John&quot;];
ages  = [37; 26; 31];
for i = 1:rows (names)
  database.(names(i,:)) = ages(i);
endfor
database
     &rArr; database =
        {
          Bill =  37
          Mary =  26
          John =  31
        }
</pre></div></div>

<p>The third way to create structures is the <code class="code">struct</code> command.  <code class="code">struct</code>
takes pairs of arguments, where the first argument in the pair is the fieldname
to include in the structure and the second is a scalar or cell array,
representing the values to include in the structure or structure array.  For
example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">struct (&quot;field1&quot;, 1, &quot;field2&quot;, 2)
&rArr; ans =
      {
        field1 =  1
        field2 =  2
      }
</pre></div></div>

<p>If the values passed to <code class="code">struct</code> are a mix of scalar and cell
arrays, then the scalar arguments are expanded to create a
structure array with a consistent dimension.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">s = struct (&quot;field1&quot;, {1, &quot;one&quot;}, &quot;field2&quot;, {2, &quot;two&quot;},
        &quot;field3&quot;, 3);
s.field1
     &rArr;
        ans =  1
        ans = one

s.field2
     &rArr;
        ans =  2
        ans = two

s.field3
     &rArr;
        ans =  3
        ans =  3
</pre></div></div>

<p>If you want to create a struct which contains a cell array as an
individual field, you must wrap it in another cell array as shown in
the following example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">struct (&quot;field1&quot;, {{1, &quot;one&quot;}}, &quot;field2&quot;, 2)
     &rArr; ans =
        {
          field1 =

        {
          [1,1] =  1
          [1,2] = one
        }

          field2 =  2
        }
</pre></div></div>

<a class="anchor" id="XREFstruct"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-struct"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">struct</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-struct"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-struct-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">struct</strong> <code class="def-code-arguments">(<var class="var">field1</var>, <var class="var">value1</var>, <var class="var">field2</var>, <var class="var">value2</var>, &hellip;)</code><a class="copiable-link" href="#index-struct-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-struct-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">struct</strong> <code class="def-code-arguments">(<var class="var">obj</var>)</code><a class="copiable-link" href="#index-struct-2"> &para;</a></span></dt>
<dd>
<p>Create a scalar or array structure and initialize its values.
</p>
<p>The <var class="var">field1</var>, <var class="var">field2</var>, &hellip; variables are strings specifying the
names of the fields and the <var class="var">value1</var>, <var class="var">value2</var>, &hellip; variables
can be of any type.
</p>
<p>If the values are cell arrays, create a structure array and initialize its
values.  The dimensions of each cell array of values must match.  Singleton
cells and non-cell values are repeated so that they fill the entire array.
If the cells are empty, create an empty structure array with the specified
field names.
</p>
<p>If the argument is an object, return the underlying struct.
</p>
<p>Observe that the syntax is optimized for struct <strong class="strong">arrays</strong>.  Consider
the following examples:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">struct (&quot;foo&quot;, 1)
  &rArr; scalar structure containing the fields:
    foo =  1

struct (&quot;foo&quot;, {})
  &rArr; 0x0 struct array containing the fields:
    foo

struct (&quot;foo&quot;, { {} })
  &rArr; scalar structure containing the fields:
    foo = {}(0x0)

struct (&quot;foo&quot;, {1, 2, 3})
  &rArr; 1x3 struct array containing the fields:
    foo

</pre></div></div>

<p>The first case is an ordinary scalar struct&mdash;one field, one value.  The
second produces an empty struct array with one field and no values, since
being passed an empty cell array of struct array values.  When the value is
a cell array containing a single entry, this becomes a scalar struct with
that single entry as the value of the field.  That single entry happens
to be an empty cell array.
</p>
<p>Finally, if the value is a nonscalar cell array, then <code class="code">struct</code>
produces a struct <strong class="strong">array</strong>.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Processing-Data-in-Cell-Arrays.html#XREFcell2struct">cell2struct</a>, <a class="ref" href="Manipulating-Structures.html#XREFfieldnames">fieldnames</a>, <a class="ref" href="Manipulating-Structures.html#XREFgetfield">getfield</a>, <a class="ref" href="Manipulating-Structures.html#XREFsetfield">setfield</a>, <a class="ref" href="Manipulating-Structures.html#XREFrmfield">rmfield</a>, <a class="ref" href="Manipulating-Structures.html#XREFisfield">isfield</a>, <a class="ref" href="Manipulating-Structures.html#XREForderfields">orderfields</a>, <a class="ref" href="#XREFisstruct">isstruct</a>, <a class="ref" href="Function-Application.html#XREFstructfun">structfun</a>.
</p></dd></dl>


<p>The function <code class="code">isstruct</code> can be used to test if an object is a
structure or a structure array.
</p>
<a class="anchor" id="XREFisstruct"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isstruct"><span class="category-def">: </span><span><code class="def-type"><var class="var">tf</var> =</code> <strong class="def-name">isstruct</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-isstruct"> &para;</a></span></dt>
<dd><p>Return true if <var class="var">x</var> is a structure or a structure array.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Predicates-for-Numeric-Objects.html#XREFismatrix">ismatrix</a>, <a class="ref" href="Basic-Usage-of-Cell-Arrays.html#XREFiscell">iscell</a>, <a class="ref" href="Built_002din-Data-Types.html#XREFisa">isa</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Manipulating-Structures.html">Manipulating Structures</a>, Previous: <a href="Structure-Arrays.html">Structure Arrays</a>, Up: <a href="Structures.html">Structures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
