<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ranges (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Ranges (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Ranges (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Numeric-Data-Types.html" rel="up" title="Numeric Data Types">
<link href="Single-Precision-Data-Types.html" rel="next" title="Single Precision Data Types">
<link href="Matrices.html" rel="prev" title="Matrices">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Ranges">
<div class="nav-panel">
<p>
Next: <a href="Single-Precision-Data-Types.html" accesskey="n" rel="next">Single Precision Data Types</a>, Previous: <a href="Matrices.html" accesskey="p" rel="prev">Matrices</a>, Up: <a href="Numeric-Data-Types.html" accesskey="u" rel="up">Numeric Data Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Ranges-1"><span>4.2 Ranges<a class="copiable-link" href="#Ranges-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-range-expressions"></a>
<a class="index-entry-id" id="index-expression_002c-range"></a>

<a class="index-entry-id" id="index-_003a_002c-range-expressions"></a>

<p>A <em class="dfn">range</em> is a convenient way to write a row vector with evenly
spaced elements.  A range expression is defined by the value of the first
element in the range, an optional value for the increment between
elements, and a maximum value which the elements of the range will not
exceed.  The base, increment, and limit are separated by colons (the
&lsquo;<samp class="samp">:</samp>&rsquo; character) and may contain any arithmetic expressions and
function calls.  If the increment is omitted, it is assumed to be 1.
For example, the range
</p>
<div class="example">
<pre class="example-preformatted">1 : 5
</pre></div>

<p>defines the set of values <code class="code">[ 1, 2, 3, 4, 5 ]</code>, and the range
</p>
<div class="example">
<pre class="example-preformatted">1 : 3 : 5
</pre></div>

<p>defines the set of values <code class="code">[ 1, 4 ]</code>.
</p>
<p>Although a range constant specifies a row vector, Octave does <em class="emph">not</em>
normally convert range constants to vectors unless it is necessary to do so.
This allows you to write a constant like <code class="code">1 : 10000</code> without using
80,000 bytes of storage on a typical workstation.
</p>
<p>A common example of when it does become necessary to convert ranges into
vectors occurs when they appear within a vector (i.e., inside square
brackets).  For instance, whereas
</p>
<div class="example">
<pre class="example-preformatted">x = 0 : 0.1 : 1;
</pre></div>

<p>defines <var class="var">x</var> to be a variable of type <code class="code">range</code> and occupies 24
bytes of memory, the expression
</p>
<div class="example">
<pre class="example-preformatted">y = [ 0 : 0.1 : 1];
</pre></div>

<p>defines <var class="var">y</var> to be of type <code class="code">matrix</code> and occupies 88 bytes of
memory.
</p>
<p>This space saving optimization may be disabled using the function
<em class="dfn">optimize_range</em>.
</p>
<a class="anchor" id="XREFoptimize_005frange"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-optimize_005frange"><span class="category-def">: </span><span><code class="def-type"><var class="var">val</var> =</code> <strong class="def-name">optimize_range</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-optimize_005frange"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-optimize_005frange-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">optimize_range</strong> <code class="def-code-arguments">(<var class="var">new_val</var>)</code><a class="copiable-link" href="#index-optimize_005frange-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-optimize_005frange-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">optimize_range</strong> <code class="def-code-arguments">(<var class="var">new_val</var>, &quot;local&quot;)</code><a class="copiable-link" href="#index-optimize_005frange-2"> &para;</a></span></dt>
<dd><p>Query or set whether a special space-efficient format is used for storing
ranges.
</p>
<p>The default value is true.  If this option is set to false, Octave will store
ranges as full matrices.
</p>
<p>When called from inside a function with the <code class="code">&quot;local&quot;</code> option, the setting
is changed locally for the function and any subroutines it calls.  The original
setting is restored when exiting the function.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Basic-Usage.html#XREFoptimize_005fdiagonal_005fmatrix">optimize_diagonal_matrix</a>, <a class="ref" href="Basic-Usage.html#XREFoptimize_005fpermutation_005fmatrix">optimize_permutation_matrix</a>.
</p></dd></dl>


<p>Note that the upper (or lower, if the increment is negative) bound on
the range is not always included in the set of values.  This can be useful
in some contexts.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">## x is some predefined range or vector or matrix or array
x(1:2:end) += 1;   # increment all  odd-numbered elements
x(2:2:end) -= 1;   # decrement all even-numbered elements
</pre></div></div>

<p>The above code works correctly whether <var class="var">x</var> has an odd number of elements
or not, so no need to treat the two cases differently.
</p>
<p>Octave uses floating point arithmetic to compute the values in the
range.  As a result, defining ranges with floating-point values can result
in pitfalls like these:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = -2
b = (0.3 - 0.2 - 0.1)
x = a : b
</pre></div></div>

<p>Due to floating point rounding, <var class="var">b</var> may or may not equal zero exactly,
and if it does not, it may be above zero or below zero, hence the final range
<var class="var">x</var> may or may not include zero as its final value.  Similarly:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">x = 1.80 : 0.05 : 1.90
y = 1.85 : 0.05 : 1.90
</pre></div></div>

<p>is not as predictable as it looks.  As of Octave 8.3, the results obtained are
that <var class="var">x</var> has three elements (1.80, 1.85, and 1.90), and <var class="var">y</var> has only
one element (1.85 but not 1.90).  Thus, when using floating points in ranges,
changing the start of the range can easily affect the end of the range even
though the ending value was not touched in the above example.
</p>
<p>To avoid such pitfalls with floating-points in ranges, you should use one of
the following patterns.  This change to the previous code:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">x = (0:2) * 0.05 + 1.80
y = (0:1) * 0.05 + 1.85
</pre></div></div>

<p>makes it much safer and much more repeatable across platforms, compilers,
and compiler settings.  If you know the number of elements, you can also use
the <code class="code">linspace</code> function (see <a class="pxref" href="Special-Utility-Matrices.html">Special Utility Matrices</a>), which will
include the endpoints of a range.  You can also make judicious use of
<code class="code">round</code>, <code class="code">floor</code>, <code class="code">ceil</code>, <code class="code">fix</code>, etc. to set the
limits and the increment without getting interference from floating-point
rounding.  For example, the earlier example can be made safer and much more
repeatable with one of the following:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = -2
b = round ((0.3 - 0.2 - 0.1) * 1e12) / 1e12   # rounds to 12 digits
c = floor (0.3 - 0.2 - 0.1)                   # floors as integer
d = floor ((0.3 - 0.2 - 0.1) * 1e12) / 1e12   # floors at 12 digits
x = a : b
y = a : c
z = a : d
</pre></div></div>

<p>When adding a scalar to a range, subtracting a scalar from it (or subtracting a
range from a scalar) and multiplying by scalar, Octave will attempt to avoid
unpacking the range and keep the result as a range, too, if it can determine
that it is safe to do so.  For instance, doing
</p>
<div class="example">
<pre class="example-preformatted">a = 2*(1:1e7) - 1;
</pre></div>

<p>will produce the same result as <code class="code">1:2:2e7-1</code>, but without ever forming a
vector with ten million elements.
</p>
<p>Using zero as an increment in the colon notation, as <code class="code">1:0:1</code> is not
allowed, because a division by zero would occur in determining the number of
range elements.  However, ranges with zero increment (i.e., all elements equal)
are useful, especially in indexing, and Octave allows them to be constructed
using the built-in function <code class="code">ones</code>.  Note that because a range must be a
row vector, <code class="code">ones (1, 10)</code> produces a range, while <code class="code">ones (10, 1)</code>
does not.
</p>
<p>When Octave parses a range expression, it examines the elements of the
expression to determine whether they are all constants.  If they are, it
replaces the range expression with a single range constant.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Single-Precision-Data-Types.html">Single Precision Data Types</a>, Previous: <a href="Matrices.html">Matrices</a>, Up: <a href="Numeric-Data-Types.html">Numeric Data Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
