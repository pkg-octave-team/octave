<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Raising Errors (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Raising Errors (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Raising Errors (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Handling-Errors.html" rel="up" title="Handling Errors">
<link href="Catching-Errors.html" rel="next" title="Catching Errors">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Raising-Errors">
<div class="nav-panel">
<p>
Next: <a href="Catching-Errors.html" accesskey="n" rel="next">Catching Errors</a>, Up: <a href="Handling-Errors.html" accesskey="u" rel="up">Handling Errors</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Raising-Errors-1"><span>12.1.1 Raising Errors<a class="copiable-link" href="#Raising-Errors-1"> &para;</a></span></h4>

<p>The most common use of errors is for checking input arguments to
functions.  The following example calls the <code class="code">error</code> function if
the function <code class="code">f</code> is called without any input arguments.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function f (arg1)
  if (nargin == 0)
    error (&quot;not enough input arguments&quot;);
  endif
endfunction
</pre></div></div>

<p>When the <code class="code">error</code> function is called, it prints the given message
and returns to the Octave prompt.  This means that no code following
a call to <code class="code">error</code> will be executed.
</p>
<p>It is also possible to assign an identification string to an error.
If an error has such an ID the user can catch this error
as will be described in the next section.  To assign an ID to an error,
simply call <code class="code">error</code> with two string arguments, where the first
is the identification string, and the second is the actual error.  Note
that error IDs are in the format <code class="code">&quot;NAMESPACE:ERROR-NAME&quot;</code>.  The namespace
<code class="code">&quot;Octave&quot;</code> is used for Octave&rsquo;s own errors.  Any other string is
available as a namespace for user&rsquo;s own errors.
</p>
<a class="anchor" id="XREFerror"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-error"><span class="category-def">: </span><span><strong class="def-name">error</strong> <code class="def-code-arguments">(<var class="var">template</var>, &hellip;)</code><a class="copiable-link" href="#index-error"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-error-1"><span class="category-def">: </span><span><strong class="def-name">error</strong> <code class="def-code-arguments">(<var class="var">id</var>, <var class="var">template</var>, &hellip;)</code><a class="copiable-link" href="#index-error-1"> &para;</a></span></dt>
<dd><p>Display an error message and stop m-file execution.
</p>
<p>Format the optional arguments under the control of the template string
<var class="var">template</var> using the same rules as the <code class="code">printf</code> family of
functions (see <a class="pxref" href="Formatted-Output.html">Formatted Output</a>) and print the resulting message
on the <code class="code">stderr</code> stream.  This formatting is only done for
single-quoted character vectors if there are additional arguments
following the template string.  If there are no additional arguments, the
template string is used literally (i.e., without interpreting any escape
sequences in single-quoted character vectors).  The message is prefixed
by &lsquo;<samp class="samp">error: </samp>&rsquo;.
</p>
<p>The optional <var class="var">id</var> argument allows programmers to tag an error
with a specific identifier so that users can later retrieve it (using
<code class="code">lasterr</code> or <code class="code">lasterror</code>) and know the origin of the error.
The identifier must contain at least one colon character (<code class="code">':'</code>)
and must not contain any whitespace characters.  It should be a string of
the form <code class="code">&quot;NAMESPACE:ERROR-NAME&quot;</code>.  Octave&rsquo;s own errors use the
<code class="code">&quot;Octave&quot;</code> namespace (see <a class="pxref" href="Catching-Errors.html#XREFerror_005fids"><code class="code">error_ids</code></a>).
For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">error (&quot;MyNameSpace:wrong-type-argument&quot;,
       &quot;fcn_name: argument should be numeric&quot;);
</pre></div></div>

<p>Calling <code class="code">error</code> also sets Octave&rsquo;s internal error state such that
control will return to the top level without evaluating any further
commands.  This is useful for aborting from functions or scripts.
</p>
<p>If the error message does not end with a newline character, Octave will
print a traceback of all the function calls leading to the error.  For
example, given the following function definitions:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function f () g (); end
function g () h (); end
function h () nargin == 1 || error (&quot;nargin != 1&quot;); end
</pre></div></div>

<p>calling the function <code class="code">f</code> will result in a list of messages that
can help you to quickly find the exact location of the error:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">f ()
error: nargin != 1
error: called from:
error:   h at line 1, column 27
error:   g at line 1, column 15
error:   f at line 1, column 15
</pre></div></div>

<p>If the error message ends in a newline character, Octave will print the
message but will not display any traceback messages as it returns
control to the top level.  For example, modifying the error message
in the previous example to end in a newline causes Octave to only print
a single message:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function h () nargin == 1 || error (&quot;nargin != 1\n&quot;); end
f ()
error: nargin != 1
</pre></div></div>

<p>A null string (&quot;&quot;) input to <code class="code">error</code> will be ignored and the code
will continue running as if the statement were a NOP.  This is for
compatibility with <small class="sc">MATLAB</small>.  It also makes it possible to write code
such as
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">err_msg = &quot;&quot;;
if (CONDITION 1)
  err_msg = &quot;CONDITION 1 found&quot;;
elseif (CONDITION2)
  err_msg = &quot;CONDITION 2 found&quot;;
...
endif
error (err_msg);
</pre></div></div>

<p>which will only stop execution if an error has been found.
</p>
<p>Implementation Note: For compatibility with <small class="sc">MATLAB</small>, escape
sequences in <var class="var">template</var> (e.g., <code class="code">&quot;\n&quot;</code> =&gt;
newline) are processed regardless of whether <var class="var">template</var> has been defined
with single quotes, as long as there are two or more input arguments.  To
disable escape sequence expansion use a second backslash before the sequence
(e.g., <code class="code">&quot;\\n&quot;</code>) or use the
<code class="code">regexptranslate</code> function.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Issuing-Warnings.html#XREFwarning">warning</a>, <a class="ref" href="Catching-Errors.html#XREFlasterror">lasterror</a>.
</p></dd></dl>


<p>Since it is common to use errors when there is something wrong with
the input to a function, Octave supports functions to simplify such code.
When the <code class="code">print_usage</code> function is called, it reads the help text
of the function calling <code class="code">print_usage</code>, and presents a useful error.
If the help text is written in Texinfo it is possible to present an
error message that only contains the function prototypes as described
by the <code class="code">@deftypefn</code> parts of the help text.  When the help text
isn&rsquo;t written in Texinfo, the error message contains the entire help
message.
</p>
<p>Consider the following function.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">## -*- texinfo -*-
## @deftypefn {} f (@var{arg1})
## Function help text goes here...
## @end deftypefn
function f (arg1)
  if (nargin == 0)
    print_usage ();
  endif
endfunction
</pre></div></div>

<p>When it is called with no input arguments it produces the following
error.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">f ()

-|  error: Invalid call to f.  Correct usage is:
-|
-|   -- f (ARG1)
-|
-|
-|  Additional help for built-in functions and operators is
-|  available in the online version of the manual.  Use the command
-|  'doc &lt;topic&gt;' to search the manual index.
-|
-|  Help and information about Octave is also available on the WWW
-|  at https://www.octave.org and via the help@octave.org
-|  mailing list.
</pre></div></div>

<a class="anchor" id="XREFprint_005fusage"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-print_005fusage"><span class="category-def">: </span><span><strong class="def-name">print_usage</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-print_005fusage"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-print_005fusage-1"><span class="category-def">: </span><span><strong class="def-name">print_usage</strong> <code class="def-code-arguments">(<var class="var">name</var>)</code><a class="copiable-link" href="#index-print_005fusage-1"> &para;</a></span></dt>
<dd><p>Print the usage message for the function <var class="var">name</var>.
</p>
<p>When called with no input arguments the <code class="code">print_usage</code> function displays
the usage message of the currently executing function.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Getting-Help.html#XREFhelp">help</a>.
</p></dd></dl>


<a class="anchor" id="XREFbeep"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-beep"><span class="category-def">: </span><span><strong class="def-name">beep</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-beep"> &para;</a></span></dt>
<dd><p>Produce a beep from the speaker (or visual bell).
</p>
<p>This function sends the alarm character <code class="code">&quot;\a&quot;</code> to
the terminal.  Depending on the user&rsquo;s configuration this may produce an
audible beep, a visual bell, or nothing at all.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Simple-Output.html#XREFputs">puts</a>, <a class="ref" href="Simple-Output.html#XREFfputs">fputs</a>, <a class="ref" href="Formatted-Output.html#XREFprintf">printf</a>, <a class="ref" href="Formatted-Output.html#XREFfprintf">fprintf</a>.
</p></dd></dl>


<a class="anchor" id="XREFbeep_005fon_005ferror"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-beep_005fon_005ferror"><span class="category-def">: </span><span><code class="def-type"><var class="var">val</var> =</code> <strong class="def-name">beep_on_error</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-beep_005fon_005ferror"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-beep_005fon_005ferror-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">beep_on_error</strong> <code class="def-code-arguments">(<var class="var">new_val</var>)</code><a class="copiable-link" href="#index-beep_005fon_005ferror-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-beep_005fon_005ferror-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">beep_on_error</strong> <code class="def-code-arguments">(<var class="var">new_val</var>, &quot;local&quot;)</code><a class="copiable-link" href="#index-beep_005fon_005ferror-2"> &para;</a></span></dt>
<dd><p>Query or set the internal variable that controls whether Octave will try
to ring the terminal bell before printing an error message.
</p>
<p>When called from inside a function with the <code class="code">&quot;local&quot;</code> option, the
variable is changed locally for the function and any subroutines it calls.
The original variable value is restored when exiting the function.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Catching-Errors.html">Catching Errors</a>, Up: <a href="Handling-Errors.html">Handling Errors</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
