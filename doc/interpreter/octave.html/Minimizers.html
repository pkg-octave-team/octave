<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Minimizers (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Minimizers (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Minimizers (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Nonlinear-Equations.html" rel="up" title="Nonlinear Equations">
<link href="Solvers.html" rel="prev" title="Solvers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Minimizers">
<div class="nav-panel">
<p>
Previous: <a href="Solvers.html" accesskey="p" rel="prev">Solvers</a>, Up: <a href="Nonlinear-Equations.html" accesskey="u" rel="up">Nonlinear Equations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Minimizers-1"><span>20.2 Minimizers<a class="copiable-link" href="#Minimizers-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-local-minimum"></a>
<a class="index-entry-id" id="index-finding-minimums"></a>

<p>Often it is useful to find the minimum value of a function rather than just
the zeroes where it crosses the x-axis.  <code class="code">fminbnd</code> is designed for the
simpler, but very common, case of a univariate function where the interval
to search is bounded.  For unbounded minimization of a function with
potentially many variables use <code class="code">fminunc</code> or <code class="code">fminsearch</code>.  The two
functions use different internal algorithms and some knowledge of the objective
function is required.  For functions which can be differentiated,
<code class="code">fminunc</code> is appropriate.  For functions with discontinuities, or for
which a gradient search would fail, use <code class="code">fminsearch</code>.
See <a class="xref" href="Optimization.html">Optimization</a> for minimization with the presence of constraint
functions.  Note that searches can be made for maxima by simply inverting the
objective function
(<code class="code">Fto_max = -Fto_min</code>).
</p>
<a class="anchor" id="XREFfminbnd"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-fminbnd"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminbnd</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">a</var>, <var class="var">b</var>)</code><a class="copiable-link" href="#index-fminbnd"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminbnd-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminbnd</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">a</var>, <var class="var">b</var>, <var class="var">options</var>)</code><a class="copiable-link" href="#index-fminbnd-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminbnd-2"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">info</var>, <var class="var">output</var>] =</code> <strong class="def-name">fminbnd</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-fminbnd-2"> &para;</a></span></dt>
<dd><p>Find a minimum point of a univariate function.
</p>
<p><var class="var">fcn</var> is a function handle, inline function, or string containing the
name of the function to evaluate.
</p>
<p>The starting interval is specified by <var class="var">a</var> (left boundary) and <var class="var">b</var>
(right boundary).  The endpoints must be finite.
</p>
<p><var class="var">options</var> is a structure specifying additional parameters which
control the algorithm.  Currently, <code class="code">fminbnd</code> recognizes these options:
<code class="code">&quot;Display&quot;</code>, <code class="code">&quot;FunValCheck&quot;</code>, <code class="code">&quot;MaxFunEvals&quot;</code>,
<code class="code">&quot;MaxIter&quot;</code>, <code class="code">&quot;OutputFcn&quot;</code>, <code class="code">&quot;TolX&quot;</code>.
</p>
<p><code class="code">&quot;MaxFunEvals&quot;</code> proscribes the maximum number of function evaluations
before optimization is halted.  The default value is 500.
The value must be a positive integer.
</p>
<p><code class="code">&quot;MaxIter&quot;</code> proscribes the maximum number of algorithm iterations
before optimization is halted.  The default value is 500.
The value must be a positive integer.
</p>
<p><code class="code">&quot;TolX&quot;</code> specifies the termination tolerance for the solution <var class="var">x</var>.
The default is <code class="code">1e-4</code>.
</p>
<p>For a description of the other options,
see <a class="pxref" href="Linear-Least-Squares.html#XREFoptimset"><code class="code">optimset</code></a>.
To initialize an options structure with default values for <code class="code">fminbnd</code>
use <code class="code">options = optimset (&quot;fminbnd&quot;)</code>.
</p>
<p>On exit, the function returns <var class="var">x</var>, the approximate minimum point, and
<var class="var">fval</var>, the function evaluated <var class="var">x</var>.
</p>
<p>The third output <var class="var">info</var> reports whether the algorithm succeeded and may
take one of the following values:
</p>
<ul class="itemize mark-bullet">
<li>1
The algorithm converged to a solution.

</li><li>0
Iteration limit (either <code class="code">MaxIter</code> or <code class="code">MaxFunEvals</code>) exceeded.

</li><li>-1
The algorithm was terminated by a user <code class="code">OutputFcn</code>.
</li></ul>

<p>Application Notes:
</p><ol class="enumerate">
<li> The search for a minimum is restricted to be in the
finite interval bound by <var class="var">a</var> and <var class="var">b</var>.  If you have only one initial
point to begin searching from then you will need to use an unconstrained
minimization algorithm such as <code class="code">fminunc</code> or <code class="code">fminsearch</code>.
<code class="code">fminbnd</code> internally uses a Golden Section search strategy.
</li><li> Use <a class="ref" href="Anonymous-Functions.html">Anonymous Functions</a> to pass additional parameters to <var class="var">fcn</var>.
For specific examples of doing so for <code class="code">fminbnd</code> and other
minimization functions see the <a class="ref" href="#Minimizers">Minimizers</a> section of the GNU Octave
manual.
</li></ol>

<p><strong class="strong">See also:</strong> <a class="ref" href="Solvers.html#XREFfzero">fzero</a>, <a class="ref" href="#XREFfminunc">fminunc</a>, <a class="ref" href="#XREFfminsearch">fminsearch</a>, <a class="ref" href="Linear-Least-Squares.html#XREFoptimset">optimset</a>.
</p></dd></dl>


<a class="anchor" id="XREFfminunc"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-fminunc"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">x0</var>)</code><a class="copiable-link" href="#index-fminunc"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">x0</var>, <var class="var">options</var>)</code><a class="copiable-link" href="#index-fminunc-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-2"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>] =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-fminunc-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">info</var>] =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-fminunc-3"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-4"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">info</var>, <var class="var">output</var>] =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-fminunc-4"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-5"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">info</var>, <var class="var">output</var>, <var class="var">grad</var>] =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-fminunc-5"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminunc-6"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">info</var>, <var class="var">output</var>, <var class="var">grad</var>, <var class="var">hess</var>] =</code> <strong class="def-name">fminunc</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-fminunc-6"> &para;</a></span></dt>
<dd><p>Solve an unconstrained optimization problem defined by the function
<var class="var">fcn</var>.
</p>
<p><code class="code">fminunc</code> attempts to determine a vector <var class="var">x</var> such that
<code class="code"><var class="var">fcn</var> (<var class="var">x</var>)</code> is a local minimum.
</p>
<p><var class="var">fcn</var> is a function handle, inline function, or string containing the
name of the function to evaluate.  <var class="var">fcn</var> should accept a vector (array)
defining the unknown variables, and return the objective function value,
optionally with gradient.
</p>
<p><var class="var">x0</var> determines a starting guess.  The shape of <var class="var">x0</var> is preserved in
all calls to <var class="var">fcn</var>, but otherwise is treated as a column vector.
</p>
<p><var class="var">options</var> is a structure specifying additional parameters which
control the algorithm.  Currently, <code class="code">fminunc</code> recognizes these options:
<code class="code">&quot;AutoScaling&quot;</code>, <code class="code">&quot;FinDiffType&quot;</code>, <code class="code">&quot;FunValCheck&quot;</code>,
<code class="code">&quot;GradObj&quot;</code>, <code class="code">&quot;MaxFunEvals&quot;</code>, <code class="code">&quot;MaxIter&quot;</code>,
<code class="code">&quot;OutputFcn&quot;</code>, <code class="code">&quot;TolFun&quot;</code>, <code class="code">&quot;TolX&quot;</code>, <code class="code">&quot;TypicalX&quot;</code>.
</p>
<p>If <code class="code">&quot;AutoScaling&quot;</code> is <code class="code">&quot;on&quot;</code>, the variables will be
automatically scaled according to the column norms of the (estimated)
Jacobian.  As a result, <code class="code">&quot;TolFun&quot;</code> becomes scaling-independent.  By
default, this option is <code class="code">&quot;off&quot;</code> because it may sometimes deliver
unexpected (though mathematically correct) results.
</p>
<p>If <code class="code">&quot;GradObj&quot;</code> is <code class="code">&quot;on&quot;</code>, it specifies that <var class="var">fcn</var>&mdash;when
called with two output arguments&mdash;also returns the Jacobian matrix of
partial first derivatives at the requested point.
</p>
<p><code class="code">&quot;MaxFunEvals&quot;</code> proscribes the maximum number of function evaluations
before optimization is halted.  The default value is
<code class="code">100 * number_of_variables</code>, i.e., <code class="code">100 * length (<var class="var">x0</var>)</code>.
The value must be a positive integer.
</p>
<p><code class="code">&quot;MaxIter&quot;</code> proscribes the maximum number of algorithm iterations
before optimization is halted.  The default value is 400.
The value must be a positive integer.
</p>
<p><code class="code">&quot;TolX&quot;</code> specifies the termination tolerance for the unknown variables
<var class="var">x</var>, while <code class="code">&quot;TolFun&quot;</code> is a tolerance for the objective function
value <var class="var">fval</var>.  The default is <code class="code">1e-6</code> for both options.
</p>
<p>For a description of the other options,
see <a class="pxref" href="Linear-Least-Squares.html#XREFoptimset"><code class="code">optimset</code></a>.
</p>
<p>On return, <var class="var">x</var> is the location of the minimum and <var class="var">fval</var> contains
the value of the objective function at <var class="var">x</var>.
</p>
<p><var class="var">info</var> may be one of the following values:
</p>
<dl class="table">
<dt>1</dt>
<dd><p>Converged to a solution point.  Relative gradient error is less than
specified by <code class="code">TolFun</code>.
</p>
</dd>
<dt>2</dt>
<dd><p>Last relative step size was less than <code class="code">TolX</code>.
</p>
</dd>
<dt>3</dt>
<dd><p>Last relative change in function value was less than <code class="code">TolFun</code>.
</p>
</dd>
<dt>0</dt>
<dd><p>Iteration limit exceeded&mdash;either maximum number of algorithm iterations
<code class="code">MaxIter</code> or maximum number of function evaluations <code class="code">MaxFunEvals</code>.
</p>
</dd>
<dt>-1</dt>
<dd><p>Algorithm terminated by <code class="code">OutputFcn</code>.
</p>
</dd>
<dt>-3</dt>
<dd><p>The trust region radius became excessively small.
</p></dd>
</dl>

<p>Optionally, <code class="code">fminunc</code> can return a structure with convergence
statistics (<var class="var">output</var>), the output gradient (<var class="var">grad</var>) at the
solution <var class="var">x</var>, and approximate Hessian (<var class="var">hess</var>) at the solution
<var class="var">x</var>.
</p>
<p>Application Notes:
</p><ol class="enumerate">
<li> If the objective function is a single nonlinear equation
of one variable then using <code class="code">fminbnd</code> is usually a better choice.
</li><li> The algorithm used by <code class="code">fminunc</code> is a gradient search which depends
on the objective function being differentiable.  If the function has
discontinuities it may be better to use a derivative-free algorithm such as
<code class="code">fminsearch</code>.
</li><li> Use <a class="ref" href="Anonymous-Functions.html">Anonymous Functions</a> to pass additional parameters to <var class="var">fcn</var>.
For specific examples of doing so for <code class="code">fminunc</code> and other
minimization functions see the <a class="ref" href="#Minimizers">Minimizers</a> section of the GNU Octave
manual.
</li></ol>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFfminbnd">fminbnd</a>, <a class="ref" href="#XREFfminsearch">fminsearch</a>, <a class="ref" href="Linear-Least-Squares.html#XREFoptimset">optimset</a>.
</p></dd></dl>


<a class="anchor" id="XREFfminsearch"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-fminsearch"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminsearch</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">x0</var>)</code><a class="copiable-link" href="#index-fminsearch"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminsearch-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminsearch</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">x0</var>, <var class="var">options</var>)</code><a class="copiable-link" href="#index-fminsearch-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminsearch-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">x</var> =</code> <strong class="def-name">fminsearch</strong> <code class="def-code-arguments">(<var class="var">problem</var>)</code><a class="copiable-link" href="#index-fminsearch-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-fminsearch-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">fval</var>, <var class="var">exitflag</var>, <var class="var">output</var>] =</code> <strong class="def-name">fminsearch</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-fminsearch-3"> &para;</a></span></dt>
<dd>
<p>Find a value of <var class="var">x</var> which minimizes the multi-variable function
<var class="var">fcn</var>.
</p>
<p><var class="var">fcn</var> is a function handle, inline function, or string containing the
name of the function to evaluate.
</p>
<p>The search begins at the point <var class="var">x0</var> and iterates using the
Nelder &amp; Mead Simplex algorithm (a derivative-free method).  This
algorithm is better-suited to functions which have discontinuities or for
which a gradient-based search such as <code class="code">fminunc</code> fails.
</p>
<p>Options for the search are provided in the parameter <var class="var">options</var> using the
function <code class="code">optimset</code>.  Currently, <code class="code">fminsearch</code> accepts the options:
<code class="code">&quot;Display&quot;</code>, <code class="code">&quot;FunValCheck&quot;</code>,<code class="code">&quot;MaxFunEvals&quot;</code>,
<code class="code">&quot;MaxIter&quot;</code>, <code class="code">&quot;OutputFcn&quot;</code>, <code class="code">&quot;TolFun&quot;</code>, <code class="code">&quot;TolX&quot;</code>.
</p>
<p><code class="code">&quot;MaxFunEvals&quot;</code> proscribes the maximum number of function evaluations
before optimization is halted.  The default value is
<code class="code">200 * number_of_variables</code>, i.e., <code class="code">200 * length (<var class="var">x0</var>)</code>.
The value must be a positive integer.
</p>
<p><code class="code">&quot;MaxIter&quot;</code> proscribes the maximum number of algorithm iterations
before optimization is halted.  The default value is
<code class="code">200 * number_of_variables</code>, i.e., <code class="code">200 * length (<var class="var">x0</var>)</code>.
The value must be a positive integer.
</p>
<p>For a description of the other options,
see <a class="pxref" href="Linear-Least-Squares.html#XREFoptimset"><code class="code">optimset</code></a>.  To initialize an options structure
with default values for <code class="code">fminsearch</code> use
<code class="code">options = optimset (&quot;fminsearch&quot;)</code>.
</p>
<p><code class="code">fminsearch</code> may also be called with a single structure argument
with the following fields:
</p>
<dl class="table">
<dt><code class="code">objective</code></dt>
<dd><p>The objective function.
</p>
</dd>
<dt><code class="code">x0</code></dt>
<dd><p>The initial point.
</p>
</dd>
<dt><code class="code">solver</code></dt>
<dd><p>Must be set to <code class="code">&quot;fminsearch&quot;</code>.
</p>
</dd>
<dt><code class="code">options</code></dt>
<dd><p>A structure returned from <code class="code">optimset</code> or an empty matrix to
indicate that defaults should be used.
</p></dd>
</dl>

<p>The field <code class="code">options</code> is optional.  All others are required.
</p>
<p>On exit, the function returns <var class="var">x</var>, the minimum point, and <var class="var">fval</var>,
the function value at the minimum.
</p>
<p>The third output <var class="var">exitflag</var> reports whether the algorithm succeeded and
may take one of the following values:
</p>
<dl class="table">
<dt>1</dt>
<dd><p>if the algorithm converged
(size of the simplex is smaller than <code class="code">TolX</code> <strong class="strong">AND</strong> the step in
function value between iterations is smaller than <code class="code">TolFun</code>).
</p>
</dd>
<dt>0</dt>
<dd><p>if the maximum number of iterations or the maximum number of function
evaluations are exceeded.
</p>
</dd>
<dt>-1</dt>
<dd><p>if the iteration is stopped by the <code class="code">&quot;OutputFcn&quot;</code>.
</p></dd>
</dl>

<p>The fourth output is a structure <var class="var">output</var> containing runtime
about the algorithm.  Fields in the structure are <code class="code">funcCount</code>
containing the number of function calls to <var class="var">fcn</var>, <code class="code">iterations</code>
containing the number of iteration steps, <code class="code">algorithm</code> with the name of
the search algorithm (always:
<code class="code">&quot;Nelder-Mead simplex direct search&quot;</code>), and <code class="code">message</code>
with the exit message.
</p>
<p>Example:
</p>
<div class="example">
<pre class="example-preformatted">fminsearch (@(x) (x(1)-5).^2+(x(2)-8).^4, [0;0])
</pre></div>

<p>Application Notes:
</p><ol class="enumerate">
<li> If you need to find the minimum of a single variable function it is
probably better to use <code class="code">fminbnd</code>.
</li><li> The legacy, undocumented syntax for passing parameters to <var class="var">fcn</var> by
appending them to the input argument list after <var class="var">options</var> is
discouraged and will be completely removed in Octave 10.  The
preferred, cross-platform compatible method of passing parameters to
<var class="var">fcn</var> is through use of <a class="ref" href="Anonymous-Functions.html">Anonymous Functions</a>.  For specific
examples of doing so for <code class="code">fminsearch</code> and other minimization
functions see the <a class="ref" href="#Minimizers">Minimizers</a> section of the GNU Octave manual.
</li></ol>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFfminbnd">fminbnd</a>, <a class="ref" href="#XREFfminunc">fminunc</a>, <a class="ref" href="Linear-Least-Squares.html#XREFoptimset">optimset</a>.
</p></dd></dl>


<p>Certain minimization operations require additional parameters be passed to
the function, <code class="code">F</code>, being minimized. E.g., <code class="code">F&nbsp;=&nbsp;F(x,&nbsp;C)</code><!-- /@w -->.
Octave&rsquo;s minimizer functions are designed to only pass one optimization
variable to <code class="code">F</code>, but parameter passing can be accomplished by defining
an <a class="ref" href="Anonymous-Functions.html">Anonymous Function</a> that contains the necessary
parameter(s).  See the example below:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A = 2; B = 3;
f = @(x) sin (A*x + B);
fminbnd (f, 0, 2)
   &rArr;   0.8562
</pre></div></div>

<p>Note that anonymous functions retain the value of parameters at the time they
are defined.  Changing a parameter value after function definition will not
affect output of the function until it is redefined:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">B = 4;
fminbnd (f, 0, 2)
   &rArr;   0.8562
f = @(x) sin (A*x + B);
fminbnd (f, 0, 2)
   &rArr;   0.3562
</pre></div></div>

<p>The function <code class="code">humps</code> is a useful function for testing zero and
extrema finding functions.
</p>
<a class="anchor" id="XREFhumps"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-humps"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">humps</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-humps"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-humps-1"><span class="category-def">: </span><span><code class="def-type">[<var class="var">x</var>, <var class="var">y</var>] =</code> <strong class="def-name">humps</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-humps-1"> &para;</a></span></dt>
<dd><p>Evaluate a function with multiple minima, maxima, and zero crossings.
</p>
<p>The output <var class="var">y</var> is the evaluation of the rational function:
</p>

<div class="example">
<div class="group"><pre class="example-preformatted">        1200*<var class="var">x</var>^4 - 2880*<var class="var">x</var>^3 + 2036*<var class="var">x</var>^2 - 348*<var class="var">x</var> - 88
 <var class="var">y</var> = - ---------------------------------------------
         200*<var class="var">x</var>^4 - 480*<var class="var">x</var>^3 + 406*<var class="var">x</var>^2 - 138*<var class="var">x</var> + 17
</pre></div></div>


<p><var class="var">x</var> may be a scalar, vector or array.  If <var class="var">x</var> is omitted, the
default range [0:0.05:1] is used.
</p>
<p>When called with two output arguments, [<var class="var">x</var>, <var class="var">y</var>], <var class="var">x</var> will
contain the input values, and <var class="var">y</var> will contain the output from
<code class="code">humps</code>.
</p>
<p>Programming Notes: <code class="code">humps</code> has two local maxima located near <var class="var">x</var> =
0.300 and 0.893, a local minimum near <var class="var">x</var> = 0.637, and zeros near
<var class="var">x</var> = -0.132 and 1.300.  <code class="code">humps</code> is a useful function for testing
algorithms which find zeros or local minima and maxima.
</p>
<p>Try <code class="code">demo humps</code> to see a plot of the <code class="code">humps</code> function.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Solvers.html#XREFfzero">fzero</a>, <a class="ref" href="#XREFfminbnd">fminbnd</a>, <a class="ref" href="#XREFfminunc">fminunc</a>, <a class="ref" href="#XREFfminsearch">fminsearch</a>.
</p></dd></dl>



</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Solvers.html">Solvers</a>, Up: <a href="Nonlinear-Equations.html">Nonlinear Equations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
