<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Function Files (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Function Files (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Function Files (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Functions-and-Scripts.html" rel="up" title="Functions and Scripts">
<link href="Script-Files.html" rel="next" title="Script Files">
<link href="Validating-Arguments.html" rel="prev" title="Validating Arguments">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Function-Files">
<div class="nav-panel">
<p>
Next: <a href="Script-Files.html" accesskey="n" rel="next">Script Files</a>, Previous: <a href="Validating-Arguments.html" accesskey="p" rel="prev">Validating Arguments</a>, Up: <a href="Functions-and-Scripts.html" accesskey="u" rel="up">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Function-Files-1"><span>11.10 Function Files<a class="copiable-link" href="#Function-Files-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-function-file"></a>

<p>Except for simple one-shot programs, it is not practical to have to
define all the functions you need each time you need them.  Instead, you
will normally want to save them in a file so that you can easily edit
them, and save them for use at a later time.
</p>
<p>Octave does not require you to load function definitions from files
before using them.  You simply need to put the function definitions in a
place where Octave can find them.
</p>
<p>When Octave encounters an identifier that is undefined, it first looks
for variables or functions that are already compiled and currently
listed in its symbol table.  If it fails to find a definition there, it
searches a list of directories (the <em class="dfn">path</em>) for files ending in
<samp class="file">.m</samp> that have the same base name as the undefined
identifier.<a class="footnote" id="DOCF5" href="#FOOT5"><sup>5</sup></a>  Once Octave finds a file with a name that matches,
the contents of the file are read.  If it defines a <em class="emph">single</em>
function, it is compiled and executed.  See <a class="xref" href="Script-Files.html">Script Files</a>, for more
information about how you can define more than one function in a single
file.
</p>
<p>When Octave defines a function from a function file, it saves the full
name of the file it read and the time stamp on the file.  If the time
stamp on the file changes, Octave may reload the file.  When Octave is
running interactively, time stamp checking normally happens at most once
each time Octave prints the prompt.  Searching for new function
definitions also occurs if the current working directory changes.
</p>
<p>Checking the time stamp allows you to edit the definition of a function
while Octave is running, and automatically use the new function
definition without having to restart your Octave session.
</p>
<p>To avoid degrading performance unnecessarily by checking the time stamps
on functions that are not likely to change, Octave assumes that function
files in the directory tree
<samp class="file"><var class="var">octave-home</var>/share/octave/<var class="var">version</var>/m</samp>
will not change, so it doesn&rsquo;t have to check their time stamps every time the
functions defined in those files are used.  This is normally a very good
assumption and provides a significant improvement in performance for the
function files that are distributed with Octave.
</p>
<p>If you know that your own function files will not change while you are
running Octave, you can improve performance by calling
<code class="code">ignore_function_time_stamp (&quot;all&quot;)</code>, so that Octave will
ignore the time stamps for all function files.  Passing
<code class="code">&quot;system&quot;</code> to this function resets the default behavior.
</p>

<a class="anchor" id="XREFedit"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-edit"><span class="category-def">: </span><span><strong class="def-name">edit</strong> <code class="def-code-arguments"><var class="var">name</var></code><a class="copiable-link" href="#index-edit"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-edit-1"><span class="category-def">: </span><span><strong class="def-name">edit</strong> <code class="def-code-arguments"><var class="var">field</var> <var class="var">value</var></code><a class="copiable-link" href="#index-edit-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-edit-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">value</var> =</code> <strong class="def-name">edit</strong> <code class="def-code-arguments">(&quot;get&quot;, <var class="var">field</var>)</code><a class="copiable-link" href="#index-edit-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-edit-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">value</var> =</code> <strong class="def-name">edit</strong> <code class="def-code-arguments">(&quot;get&quot;, &quot;all&quot;)</code><a class="copiable-link" href="#index-edit-3"> &para;</a></span></dt>
<dd><p>Edit the named function, or change editor settings.
</p>
<p>If <code class="code">edit</code> is called with the name of a file or function as its
argument it will be opened in the default text editor.  The default editor
for the Octave GUI is specified in the Editor tab of Preferences.  The
default editor for the CLI is specified by the <code class="code">EDITOR</code> function.
</p>
<ul class="itemize mark-bullet">
<li>If the function <var class="var">name</var> is available in a file on your path, then it
will be opened in the editor.  If no file is found, then the m-file
variant, ending with <code class="code">&quot;.m&quot;</code>, will be considered.  If still no file is
found, then variants with a leading <code class="code">&quot;@&quot;</code> and then with both a
leading <code class="code">&quot;@&quot;</code> and trailing <code class="code">&quot;.m&quot;</code> will be considered.

</li><li>If <var class="var">name</var> is the name of a command-line function, then an m-file will
be created to contain that function along with its current definition.

</li><li>If <code class="code"><var class="var">name</var>.cc</code> is specified, then it will search for
<samp class="file"><var class="var">name</var>.cc</samp> in the path and open it in the editor.  If the file is
not found, then a new <samp class="file">.cc</samp> file will be created.  If <var class="var">name</var>
happens to be an m-file or command-line function, then the text of that
function will be inserted into the .cc file as a comment.

</li><li>If <samp class="file"><var class="var">name</var>.ext</samp> is on your path then it will be edited, otherwise
the editor will be started with <samp class="file"><var class="var">name</var>.ext</samp> in the current
directory as the filename.

<p><strong class="strong">Warning:</strong> You may need to clear <var class="var">name</var> before the new definition
is available.  If you are editing a .cc file, you will need to execute
<code class="code">mkoctfile <samp class="file"><var class="var">name</var>.cc</samp></code> before the definition will be
available.
</p></li></ul>

<p>If <code class="code">edit</code> is called with <var class="var">field</var> and <var class="var">value</var> variables, the
value of the control field <var class="var">field</var> will be set to <var class="var">value</var>.
</p>
<p>If an output argument is requested and the first input argument is
<code class="code">get</code> then <code class="code">edit</code> will return the value of the control field
<var class="var">field</var>.  If the control field does not exist, edit will return a
structure containing all fields and values.  Thus, <code class="code">edit (&quot;get&quot;,
<code class="code">&quot;all&quot;</code>)</code> returns a complete control structure.
</p>
<p>The following control fields are used:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">author</samp>&rsquo;</dt>
<dd><p>This is the name to put after the &quot;## Author:&quot; field of new functions.  By
default it guesses from the <code class="code">gecos</code> field of the password database.
</p>
</dd>
<dt>&lsquo;<samp class="samp">email</samp>&rsquo;</dt>
<dd><p>This is the e-mail address to list after the name in the author field.  By
default it guesses <code class="code">&lt;$LOGNAME@$HOSTNAME&gt;</code>, and if <code class="code">$HOSTNAME</code>
is not defined it uses <code class="code">uname -n</code>.  You probably want to override
this.  Be sure to use the format <code class="code"><a class="email" href="mailto:user@host">user@host</a></code>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">license</samp>&rsquo;</dt>
<dd>
<dl class="table">
<dt>&lsquo;<samp class="samp">gpl</samp>&rsquo;</dt>
<dd><p>GNU General Public License (default).
</p>
</dd>
<dt>&lsquo;<samp class="samp">bsd</samp>&rsquo;</dt>
<dd><p>BSD-style license without advertising clause.
</p>
</dd>
<dt>&lsquo;<samp class="samp">pd</samp>&rsquo;</dt>
<dd><p>Public domain.
</p>
</dd>
<dt>&lsquo;<samp class="samp">&quot;text&quot;</samp>&rsquo;</dt>
<dd><p>Your own default copyright and license.
</p></dd>
</dl>

<p>Unless you specify &lsquo;<samp class="samp">pd</samp>&rsquo;, edit will prepend the copyright statement
with &quot;Copyright (C) YYYY Author&quot;.
</p>
</dd>
<dt>&lsquo;<samp class="samp">mode</samp>&rsquo;</dt>
<dd><p>This value determines whether the editor should be started in async mode
(editor is started in the background and Octave continues) or sync mode
(Octave waits until the editor exits).  Set it to <code class="code">&quot;sync&quot;</code> to start
the editor in sync mode.  The default is <code class="code">&quot;async&quot;</code>
(see <a class="pxref" href="Controlling-Subprocesses.html#XREFsystem"><code class="code">system</code></a>).
</p>
</dd>
<dt>&lsquo;<samp class="samp">editinplace</samp>&rsquo;</dt>
<dd><p>Determines whether files should be edited in place, without regard to
whether they are modifiable or not.  The default is <code class="code">true</code>.
Set it to <code class="code">false</code> to have read-only function files automatically
copied to &lsquo;<samp class="samp">home</samp>&rsquo;, if it exists, when editing them.
</p>
</dd>
<dt>&lsquo;<samp class="samp">home</samp>&rsquo;</dt>
<dd><p>This value indicates a directory that system m-files should be copied into
before opening them in the editor.  The intent is that this directory is
also in the path, so that the edited copy of a system function file shadows
the original.  This setting only has an effect when &lsquo;<samp class="samp">editinplace</samp>&rsquo; is
set to <code class="code">false</code>.  The default is the empty matrix (<code class="code">[]</code>), which
means it is not used.  The default in previous versions of Octave was
<samp class="file">~/octave</samp>.
</p></dd>
</dl>

<p><strong class="strong">See also:</strong> <a class="ref" href="Commands-for-History.html#XREFEDITOR">EDITOR</a>, <a class="ref" href="Manipulating-the-Load-Path.html#XREFpath">path</a>.
</p></dd></dl>


<a class="anchor" id="XREFmfilename"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-mfilename"><span class="category-def">: </span><span><strong class="def-name">mfilename</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-mfilename"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-mfilename-1"><span class="category-def">: </span><span><strong class="def-name">mfilename</strong> <code class="def-code-arguments">(&quot;fullpath&quot;)</code><a class="copiable-link" href="#index-mfilename-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-mfilename-2"><span class="category-def">: </span><span><strong class="def-name">mfilename</strong> <code class="def-code-arguments">(&quot;fullpathext&quot;)</code><a class="copiable-link" href="#index-mfilename-2"> &para;</a></span></dt>
<dd><p>Return the name of the currently executing file.
</p>
<p>The base name of the currently executing script or function is returned without
any extension.  If called from outside an m-file, such as the command line,
return the empty string.
</p>
<p>Given the argument <code class="code">&quot;fullpath&quot;</code>, include the directory part of the
filename, but not the extension.
</p>
<p>Given the argument <code class="code">&quot;fullpathext&quot;</code>, include the directory part of
the filename and the extension.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Defining-Functions.html#XREFinputname">inputname</a>, <a class="ref" href="Call-Stack.html#XREFdbstack">dbstack</a>.
</p></dd></dl>


<a class="anchor" id="XREFignore_005ffunction_005ftime_005fstamp"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-ignore_005ffunction_005ftime_005fstamp"><span class="category-def">: </span><span><code class="def-type"><var class="var">val</var> =</code> <strong class="def-name">ignore_function_time_stamp</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-ignore_005ffunction_005ftime_005fstamp"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-ignore_005ffunction_005ftime_005fstamp-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">ignore_function_time_stamp</strong> <code class="def-code-arguments">(<var class="var">new_val</var>)</code><a class="copiable-link" href="#index-ignore_005ffunction_005ftime_005fstamp-1"> &para;</a></span></dt>
<dd><p>Query or set the internal variable that controls whether Octave checks
the time stamp on files each time it looks up functions defined in
function files.
</p>
<p>If the internal variable is set to <code class="code">&quot;system&quot;</code>, Octave will not
automatically recompile function files in subdirectories of
<samp class="file"><var class="var">octave-home</var>/share/<var class="var">version</var>/m</samp> if they have changed since
they were last compiled, but will recompile other function files in the
search path if they change.
</p>
<p>If set to <code class="code">&quot;all&quot;</code>, Octave will not recompile any function files
unless their definitions are removed with <code class="code">clear</code>.
</p>
<p>If set to <code class="code">&quot;none&quot;</code>, Octave will always check time stamps on files
to determine whether functions defined in function files need to
recompiled.
</p></dd></dl>



<ul class="mini-toc">
<li><a href="Manipulating-the-Load-Path.html" accesskey="1">Manipulating the Load Path</a></li>
<li><a href="Subfunctions.html" accesskey="2">Subfunctions</a></li>
<li><a href="Private-Functions.html" accesskey="3">Private Functions</a></li>
<li><a href="Nested-Functions.html" accesskey="4">Nested Functions</a></li>
<li><a href="Overloading-and-Autoloading.html" accesskey="5">Overloading and Autoloading</a></li>
<li><a href="Function-Locking.html" accesskey="6">Function Locking</a></li>
<li><a href="Function-Precedence.html" accesskey="7">Function Precedence</a></li>
</ul>
</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT5" href="#DOCF5">(5)</a></h5>
<p>The &lsquo;<samp class="samp">.m</samp>&rsquo; suffix was chosen for compatibility
with <small class="sc">MATLAB</small>.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Script-Files.html">Script Files</a>, Previous: <a href="Validating-Arguments.html">Validating Arguments</a>, Up: <a href="Functions-and-Scripts.html">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
