<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>The switch Statement (GNU Octave (version 9.4.0))</title>

<meta name="description" content="The switch Statement (GNU Octave (version 9.4.0))">
<meta name="keywords" content="The switch Statement (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Statements.html" rel="up" title="Statements">
<link href="The-while-Statement.html" rel="next" title="The while Statement">
<link href="The-if-Statement.html" rel="prev" title="The if Statement">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="The-switch-Statement">
<div class="nav-panel">
<p>
Next: <a href="The-while-Statement.html" accesskey="n" rel="next">The while Statement</a>, Previous: <a href="The-if-Statement.html" accesskey="p" rel="prev">The if Statement</a>, Up: <a href="Statements.html" accesskey="u" rel="up">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="The-switch-Statement-1"><span>10.2 The switch Statement<a class="copiable-link" href="#The-switch-Statement-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-switch-statement"></a>
<a class="index-entry-id" id="index-case-statement"></a>
<a class="index-entry-id" id="index-otherwise-statement"></a>
<a class="index-entry-id" id="index-endswitch-statement"></a>

<p>It is very common to take different actions depending on the value of
one variable.  This is possible using the <code class="code">if</code> statement in the
following way
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (X == 1)
  do_something ();
elseif (X == 2)
  do_something_else ();
else
  do_something_completely_different ();
endif
</pre></div></div>

<p>This kind of code can however be very cumbersome to both write and
maintain.  To overcome this problem Octave supports the <code class="code">switch</code>
statement.  Using this statement, the above example becomes
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">switch (X)
  case 1
    do_something ();
  case 2
    do_something_else ();
  otherwise
    do_something_completely_different ();
endswitch
</pre></div></div>

<p>This code makes the repetitive structure of the problem more explicit,
making the code easier to read, and hence maintain.  Also, if the
variable <code class="code">X</code> should change its name, only one line would need
changing compared to one line per case when <code class="code">if</code> statements are
used.
</p>
<p>The general form of the <code class="code">switch</code> statement is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">switch (<var class="var">expression</var>)
  case <var class="var">label</var>
    <var class="var">command_list</var>
  case <var class="var">label</var>
    <var class="var">command_list</var>
  ...

  otherwise
    <var class="var">command_list</var>
endswitch
</pre></div></div>

<p>where <var class="var">label</var> can be any expression.  However, duplicate
<var class="var">label</var> values are not detected, and only the <var class="var">command_list</var>
corresponding to the first match will be executed.  For the
<code class="code">switch</code> statement to be meaningful at least one
<code class="code">case <var class="var">label</var> <var class="var">command_list</var></code> clause must be present,
while the <code class="code">otherwise <var class="var">command_list</var></code> clause is optional.
</p>
<p>If <var class="var">label</var> is a cell array the corresponding <var class="var">command_list</var>
is executed if <em class="emph">any</em> of the elements of the cell array match
<var class="var">expression</var>.  As an example, the following program will print
&lsquo;<samp class="samp">Variable is either 6 or 7</samp>&rsquo;.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A = 7;
switch (A)
  case { 6, 7 }
    printf (&quot;variable is either 6 or 7\n&quot;);
  otherwise
    printf (&quot;variable is neither 6 nor 7\n&quot;);
endswitch
</pre></div></div>

<p>As with all other specific <code class="code">end</code> keywords, <code class="code">endswitch</code> may be
replaced by <code class="code">end</code>, but you can get better diagnostics if you use
the specific forms.
</p>

<p>One advantage of using the <code class="code">switch</code> statement compared to using
<code class="code">if</code> statements is that the <var class="var">label</var>s can be strings.  If an
<code class="code">if</code> statement is used it is <em class="emph">not</em> possible to write
</p>
<div class="example">
<pre class="example-preformatted">if (X == &quot;a string&quot;) # This is NOT valid
</pre></div>

<p>since a character-to-character comparison between <code class="code">X</code> and the
string will be made instead of evaluating if the strings are equal.
This special-case is handled by the <code class="code">switch</code> statement, and it
is possible to write programs that look like this
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">switch (X)
  case &quot;a string&quot;
    do_something
  ...
endswitch
</pre></div></div>


<ul class="mini-toc">
<li><a href="Notes-for-the-C-Programmer.html" accesskey="1">Notes for the C Programmer</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="The-while-Statement.html">The while Statement</a>, Previous: <a href="The-if-Statement.html">The if Statement</a>, Up: <a href="Statements.html">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
