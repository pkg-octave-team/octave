<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Overloading and Autoloading (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Overloading and Autoloading (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Overloading and Autoloading (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Function-Files.html" rel="up" title="Function Files">
<link href="Function-Locking.html" rel="next" title="Function Locking">
<link href="Nested-Functions.html" rel="prev" title="Nested Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Overloading-and-Autoloading">
<div class="nav-panel">
<p>
Next: <a href="Function-Locking.html" accesskey="n" rel="next">Function Locking</a>, Previous: <a href="Nested-Functions.html" accesskey="p" rel="prev">Nested Functions</a>, Up: <a href="Function-Files.html" accesskey="u" rel="up">Function Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Overloading-and-Autoloading-1"><span>11.10.5 Overloading and Autoloading<a class="copiable-link" href="#Overloading-and-Autoloading-1"> &para;</a></span></h4>

<p>Functions can be overloaded to work with different input arguments.  For
example, the operator &rsquo;+&rsquo; has been overloaded in Octave to work with single,
double, uint8, int32, and many other arguments.  The preferred way to overload
functions is through classes and object oriented programming
(see <a class="pxref" href="Function-Overloading.html">Function Overloading</a>).  Occasionally, however, one needs to undo
user overloading and call the default function associated with a specific
type.  The <code class="code">builtin</code> function exists for this purpose.
</p>
<a class="anchor" id="XREFbuiltin"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-builtin"><span class="category-def">: </span><span><code class="def-type">[&hellip;] =</code> <strong class="def-name">builtin</strong> <code class="def-code-arguments">(<var class="var">f</var>, &hellip;)</code><a class="copiable-link" href="#index-builtin"> &para;</a></span></dt>
<dd><p>Call the base function <var class="var">f</var> even if <var class="var">f</var> is overloaded to another
function for the given type signature.
</p>
<p>This is normally useful when doing object-oriented programming and there is
a requirement to call one of Octave&rsquo;s base functions rather than the
overloaded one of a new class.
</p>
<p>A trivial example which redefines the <code class="code">sin</code> function to be the
<code class="code">cos</code> function shows how <code class="code">builtin</code> works.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">sin (0)
  &rArr; 0
function y = sin (x), y = cos (x); endfunction
sin (0)
  &rArr; 1
builtin (&quot;sin&quot;, 0)
  &rArr; 0
</pre></div></div>
</dd></dl>


<p>A single dynamically linked file might define several
functions.  However, as Octave searches for functions based on the
functions filename, Octave needs a manner in which to find each of the
functions in the dynamically linked file.  On operating systems that
support symbolic links, it is possible to create a symbolic link to the
original file for each of the functions which it contains.
</p>
<p>However, there is at least one well known operating system that doesn&rsquo;t
support symbolic links.  Making copies of the original file for each of
the functions is undesirable as it increases the
amount of disk space used by Octave.  Instead Octave supplies the
<code class="code">autoload</code> function, that permits the user to define in which
file a certain function will be found.
</p>
<a class="anchor" id="XREFautoload"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-autoload"><span class="category-def">: </span><span><code class="def-type"><var class="var">autoload_map</var> =</code> <strong class="def-name">autoload</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-autoload"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-autoload-1"><span class="category-def">: </span><span><strong class="def-name">autoload</strong> <code class="def-code-arguments">(<var class="var">function</var>, <var class="var">file</var>)</code><a class="copiable-link" href="#index-autoload-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-autoload-2"><span class="category-def">: </span><span><strong class="def-name">autoload</strong> <code class="def-code-arguments">(&hellip;, &quot;remove&quot;)</code><a class="copiable-link" href="#index-autoload-2"> &para;</a></span></dt>
<dd><p>Define <var class="var">function</var> to autoload from <var class="var">file</var>.
</p>
<p>The second argument, <var class="var">file</var>, should be an absolute filename or a file
name in the same directory as the function or script from which the autoload
command was run.  <var class="var">file</var> <em class="emph">should not</em> depend on the Octave load
path.
</p>
<p>Normally, calls to <code class="code">autoload</code> appear in PKG_ADD script files that are
evaluated when a directory is added to Octave&rsquo;s load path.  To avoid having
to hardcode directory names in <var class="var">file</var>, if <var class="var">file</var> is in the same
directory as the PKG_ADD script then
</p>
<div class="example">
<pre class="example-preformatted">autoload (&quot;foo&quot;, &quot;bar.oct&quot;);
</pre></div>

<p>will load the function <code class="code">foo</code> from the file <code class="code">bar.oct</code>.  The above
usage when <code class="code">bar.oct</code> is not in the same directory, or usages such as
</p>
<div class="example">
<pre class="example-preformatted">autoload (&quot;foo&quot;, file_in_loadpath (&quot;bar.oct&quot;))
</pre></div>

<p>are strongly discouraged, as their behavior may be unpredictable.
</p>
<p>With no arguments, return a structure containing the current autoload map.
</p>
<p>If a third argument <code class="code">&quot;remove&quot;</code> is given, the function is cleared and
not loaded anymore during the current Octave session.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Creating-Packages.html#XREFPKG_005fADD">PKG_ADD</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Function-Locking.html">Function Locking</a>, Previous: <a href="Nested-Functions.html">Nested Functions</a>, Up: <a href="Function-Files.html">Function Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
