<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Recursion (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Recursion (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Recursion (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Calling-Functions.html" rel="up" title="Calling Functions">
<link href="Access-via-Handle.html" rel="next" title="Access via Handle">
<link href="Call-by-Value.html" rel="prev" title="Call by Value">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Recursion">
<div class="nav-panel">
<p>
Next: <a href="Access-via-Handle.html" accesskey="n" rel="next">Access via Handle</a>, Previous: <a href="Call-by-Value.html" accesskey="p" rel="prev">Call by Value</a>, Up: <a href="Calling-Functions.html" accesskey="u" rel="up">Calling Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Recursion-1"><span>8.2.2 Recursion<a class="copiable-link" href="#Recursion-1"> &para;</a></span></h4>
<a class="index-entry-id" id="index-factorial-function"></a>

<p>With some restrictions<a class="footnote" id="DOCF3" href="#FOOT3"><sup>3</sup></a>, recursive function calls are allowed.  A
<em class="dfn">recursive function</em> is one which calls itself, either directly or
indirectly.  For example, here is an inefficient<a class="footnote" id="DOCF4" href="#FOOT4"><sup>4</sup></a> way to compute the factorial of a given integer:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function retval = fact (n)
  if (n &gt; 0)
    retval = n * fact (n-1);
  else
    retval = 1;
  endif
endfunction
</pre></div></div>

<p>This function is recursive because it calls itself directly.  It
eventually terminates because each time it calls itself, it uses an
argument that is one less than was used for the previous call.  Once the
argument is no longer greater than zero, it does not call itself, and
the recursion ends.
</p>
<p>The function <code class="code">max_recursion_depth</code> may be used to specify a limit
to the recursion depth and prevents Octave from recursing infinitely.
Similarly, the function <code class="code">max_stack_depth</code> may be used to specify
limit to the depth of function calls, whether recursive or not.  These
limits help prevent stack overflow on the computer Octave is running on,
so that instead of exiting with a signal, the interpreter will throw an
error and return to the command prompt.
</p>
<a class="anchor" id="XREFmax_005frecursion_005fdepth"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-max_005frecursion_005fdepth"><span class="category-def">: </span><span><code class="def-type"><var class="var">val</var> =</code> <strong class="def-name">max_recursion_depth</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-max_005frecursion_005fdepth"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-max_005frecursion_005fdepth-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">max_recursion_depth</strong> <code class="def-code-arguments">(<var class="var">new_val</var>)</code><a class="copiable-link" href="#index-max_005frecursion_005fdepth-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-max_005frecursion_005fdepth-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">max_recursion_depth</strong> <code class="def-code-arguments">(<var class="var">new_val</var>, &quot;local&quot;)</code><a class="copiable-link" href="#index-max_005frecursion_005fdepth-2"> &para;</a></span></dt>
<dd><p>Query or set the internal limit on the number of times a function may
be called recursively.
</p>
<p>If the limit is exceeded, an error message is printed and control returns to
the top level.
</p>
<p>When called from inside a function with the <code class="code">&quot;local&quot;</code> option, the
variable is changed locally for the function and any subroutines it calls.
The original variable value is restored when exiting the function.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFmax_005fstack_005fdepth">max_stack_depth</a>.
</p></dd></dl>


<a class="anchor" id="XREFmax_005fstack_005fdepth"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-max_005fstack_005fdepth"><span class="category-def">: </span><span><code class="def-type"><var class="var">val</var> =</code> <strong class="def-name">max_stack_depth</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-max_005fstack_005fdepth"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-max_005fstack_005fdepth-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">max_stack_depth</strong> <code class="def-code-arguments">(<var class="var">new_val</var>)</code><a class="copiable-link" href="#index-max_005fstack_005fdepth-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-max_005fstack_005fdepth-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">old_val</var> =</code> <strong class="def-name">max_stack_depth</strong> <code class="def-code-arguments">(<var class="var">new_val</var>, &quot;local&quot;)</code><a class="copiable-link" href="#index-max_005fstack_005fdepth-2"> &para;</a></span></dt>
<dd><p>Query or set the internal limit on the number of times a function may
be called recursively.
</p>
<p>If the limit is exceeded, an error message is printed and control returns to
the top level.
</p>
<p>When called from inside a function with the <code class="code">&quot;local&quot;</code> option, the
variable is changed locally for the function and any subroutines it calls.
The original variable value is restored when exiting the function.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFmax_005frecursion_005fdepth">max_recursion_depth</a>.
</p></dd></dl>


</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT3" href="#DOCF3">(3)</a></h5>
<p>Some of Octave&rsquo;s functions are
implemented in terms of functions that cannot be called recursively.
For example, the ODE solver <code class="code">lsode</code> is ultimately implemented in a
Fortran subroutine that cannot be called recursively, so <code class="code">lsode</code>
should not be called either directly or indirectly from within the
user-supplied function that <code class="code">lsode</code> requires.  Doing so will result
in an error.</p>
<h5 class="footnote-body-heading"><a id="FOOT4" href="#DOCF4">(4)</a></h5>
<p>It would be
much better to use <code class="code">prod (1:n)</code>, or <code class="code">gamma (n+1)</code> instead,
after first checking to ensure that the value <code class="code">n</code> is actually a
positive integer.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Access-via-Handle.html">Access via Handle</a>, Previous: <a href="Call-by-Value.html">Call by Value</a>, Up: <a href="Calling-Functions.html">Calling Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
