<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Script Files (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Script Files (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Script Files (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Functions-and-Scripts.html" rel="up" title="Functions and Scripts">
<link href="Function-Handles-and-Anonymous-Functions.html" rel="next" title="Function Handles and Anonymous Functions">
<link href="Function-Files.html" rel="prev" title="Function Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Script-Files">
<div class="nav-panel">
<p>
Next: <a href="Function-Handles-and-Anonymous-Functions.html" accesskey="n" rel="next">Function Handles and Anonymous Functions</a>, Previous: <a href="Function-Files.html" accesskey="p" rel="prev">Function Files</a>, Up: <a href="Functions-and-Scripts.html" accesskey="u" rel="up">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Script-Files-1"><span>11.11 Script Files<a class="copiable-link" href="#Script-Files-1"> &para;</a></span></h3>

<p>A script file is a file containing (almost) any sequence of Octave
commands.  It is read and evaluated just as if you had typed each
command at the Octave prompt, and provides a convenient way to perform a
sequence of commands that do not logically belong inside a function.
</p>
<p>Unlike a function file, a script file must <em class="emph">not</em> begin with the
keyword <code class="code">function</code>.  If it does, Octave will assume that it is a
function file, and that it defines a single function that should be
evaluated as soon as it is defined.
</p>
<p>A script file also differs from a function file in that the variables
named in a script file are not local variables, but are in the same
scope as the other variables that are visible on the command line.
</p>
<p>Even though a script file may not begin with the <code class="code">function</code>
keyword, it is possible to define more than one function in a single
script file and load (but not execute) all of them at once.  To do
this, the first token in the file (ignoring comments and other white
space) must be something other than <code class="code">function</code>.  If you have no
other statements to evaluate, you can use a statement that has no
effect, like this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"># Prevent Octave from thinking that this
# is a function file:

1;

# Define function one:

function one ()
  ...
</pre></div></div>

<p>To have Octave read and compile these functions into an internal form,
you need to make sure that the file is in Octave&rsquo;s load path
(accessible through the <code class="code">path</code> function), then simply type the
base name of the file that contains the commands.  (Octave uses the
same rules to search for script files as it does to search for
function files.)
</p>
<p>If the first token in a file (ignoring comments) is <code class="code">function</code>,
Octave will compile the function and try to execute it, printing a
message warning about any non-whitespace characters that appear after
the function definition.
</p>
<p>Note that Octave does not try to look up the definition of any identifier
until it needs to evaluate it.  This means that Octave will compile the
following statements if they appear in a script file, or are typed at
the command line,
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"># not a function file:
1;
function foo ()
  do_something ();
endfunction
function do_something ()
  do_something_else ();
endfunction
</pre></div></div>

<p>even though the function <code class="code">do_something</code> is not defined before it is
referenced in the function <code class="code">foo</code>.  This is not an error because
Octave does not need to resolve all symbols that are referenced by a
function until the function is actually evaluated.
</p>
<p>Since Octave doesn&rsquo;t look for definitions until they are needed, the
following code will always print &lsquo;<samp class="samp">bar = 3</samp>&rsquo; whether it is typed
directly on the command line, read from a script file, or is part of a
function body, even if there is a function or script file called
<samp class="file">bar.m</samp> in Octave&rsquo;s path.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">eval (&quot;bar = 3&quot;);
bar
</pre></div></div>

<p>Code like this appearing within a function body could fool Octave if
definitions were resolved as the function was being compiled.  It would
be virtually impossible to make Octave clever enough to evaluate this
code in a consistent fashion.  The parser would have to be able to
perform the call to <code class="code">eval</code> at compile time, and that would be
impossible unless all the references in the string to be evaluated could
also be resolved, and requiring that would be too restrictive (the
string might come from user input, or depend on things that are not
known until the function is evaluated).
</p>
<p>Although Octave normally executes commands from script files that have
the name <samp class="file"><var class="var">file</var>.m</samp>, you can use the function <code class="code">source</code> to
execute commands from any file.
</p>
<a class="anchor" id="XREFsource"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-source"><span class="category-def">: </span><span><strong class="def-name">source</strong> <code class="def-code-arguments">(<var class="var">file</var>)</code><a class="copiable-link" href="#index-source"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-source-1"><span class="category-def">: </span><span><strong class="def-name">source</strong> <code class="def-code-arguments">(<var class="var">file</var>, <var class="var">context</var>)</code><a class="copiable-link" href="#index-source-1"> &para;</a></span></dt>
<dd><p>Parse and execute the contents of <var class="var">file</var>.
</p>
<p>Without specifying <var class="var">context</var>, this is equivalent to executing commands
from a script file, but without requiring the file to be named
<samp class="file"><var class="var">file</var>.m</samp> or to be on the execution path.
</p>
<p>Instead of the current context, the script may be executed in either the
context of the function that called the present function
(<code class="code">&quot;caller&quot;</code>), or the top-level context (<code class="code">&quot;base&quot;</code>).
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Calling-a-Function-by-its-Name.html#XREFrun">run</a>.
</p></dd></dl>



<ul class="mini-toc">
<li><a href="Publish-Octave-Script-Files.html" accesskey="1">Publish Octave Script Files</a></li>
<li><a href="Publishing-Markup.html" accesskey="2">Publishing Markup</a></li>
<li><a href="Jupyter-Notebooks.html" accesskey="3">Jupyter Notebooks</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Function-Handles-and-Anonymous-Functions.html">Function Handles and Anonymous Functions</a>, Previous: <a href="Function-Files.html">Function Files</a>, Up: <a href="Functions-and-Scripts.html">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
