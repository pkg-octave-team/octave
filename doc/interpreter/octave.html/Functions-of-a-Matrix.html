<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Functions of a Matrix (GNU Octave (version 9.4.0))</title>

<meta name="description" content="Functions of a Matrix (GNU Octave (version 9.4.0))">
<meta name="keywords" content="Functions of a Matrix (GNU Octave (version 9.4.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Linear-Algebra.html" rel="up" title="Linear Algebra">
<link href="Specialized-Solvers.html" rel="next" title="Specialized Solvers">
<link href="Matrix-Factorizations.html" rel="prev" title="Matrix Factorizations">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Functions-of-a-Matrix">
<div class="nav-panel">
<p>
Next: <a href="Specialized-Solvers.html" accesskey="n" rel="next">Specialized Solvers</a>, Previous: <a href="Matrix-Factorizations.html" accesskey="p" rel="prev">Matrix Factorizations</a>, Up: <a href="Linear-Algebra.html" accesskey="u" rel="up">Linear Algebra</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Functions-of-a-Matrix-1"><span>18.4 Functions of a Matrix<a class="copiable-link" href="#Functions-of-a-Matrix-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-matrix_002c-functions-of"></a>

<a class="anchor" id="XREFexpm"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-expm"><span class="category-def">: </span><span><code class="def-type"><var class="var">r</var> =</code> <strong class="def-name">expm</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-expm"> &para;</a></span></dt>
<dd><p>Return the exponential of a matrix.
</p>
<p>The matrix exponential is defined as the infinite Taylor series
</p>
<div class="example">
<pre class="example-preformatted">expm (A) = I + A + A^2/2! + A^3/3! + ...
</pre></div>

<p>However, the Taylor series is <em class="emph">not</em> the way to compute the matrix
exponential; see Moler and Van Loan, <cite class="cite">Nineteen Dubious Ways
to Compute the Exponential of a Matrix</cite>, SIAM Review, 1978.  This routine
uses Ward&rsquo;s diagonal Pad&eacute; approximation method with three step
preconditioning (SIAM Journal on Numerical Analysis, 1977).  Diagonal
Pad&eacute; approximations are rational polynomials of matrices
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">     -1
D (A)   N (A)
</pre></div></div>

<p>whose Taylor series matches the first
<code class="code">2q+1</code>
terms of the Taylor series above; direct evaluation of the Taylor series
(with the same preconditioning steps) may be desirable in lieu of the
Pad&eacute; approximation when
<code class="code">Dq(A)</code>
is ill-conditioned.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFlogm">logm</a>, <a class="ref" href="#XREFsqrtm">sqrtm</a>.
</p></dd></dl>


<a class="anchor" id="XREFlogm"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-logm"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">logm</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-logm"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-logm-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">logm</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">opt_iters</var>)</code><a class="copiable-link" href="#index-logm-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-logm-2"><span class="category-def">: </span><span><code class="def-type">[<var class="var">s</var>, <var class="var">iters</var>] =</code> <strong class="def-name">logm</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-logm-2"> &para;</a></span></dt>
<dd><p>Compute the matrix logarithm of the square matrix <var class="var">A</var>.
</p>
<p>The implementation utilizes a Pad&eacute; approximant and the identity
</p>
<div class="example">
<pre class="example-preformatted">logm (<var class="var">A</var>) = 2^k * logm (<var class="var">A</var>^(1 / 2^k))
</pre></div>

<p>The optional input <var class="var">opt_iters</var> is the maximum number of square roots
to compute and defaults to 100.
</p>
<p>The optional output <var class="var">iters</var> is the number of square roots actually
computed.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFexpm">expm</a>, <a class="ref" href="#XREFsqrtm">sqrtm</a>.
</p></dd></dl>


<a class="anchor" id="XREFsqrtm"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-sqrtm"><span class="category-def">: </span><span><code class="def-type"><var class="var">s</var> =</code> <strong class="def-name">sqrtm</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-sqrtm"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-sqrtm-1"><span class="category-def">: </span><span><code class="def-type">[<var class="var">s</var>, <var class="var">error_estimate</var>] =</code> <strong class="def-name">sqrtm</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-sqrtm-1"> &para;</a></span></dt>
<dd><p>Compute the matrix square root of the square matrix <var class="var">A</var>.
</p>
<p>Ref: N.J. Higham.  <cite class="cite">A New sqrtm for <small class="sc">MATLAB</small></cite>.  Numerical
Analysis Report No. 336, Manchester Centre for Computational
Mathematics, Manchester, England, January 1999.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFexpm">expm</a>, <a class="ref" href="#XREFlogm">logm</a>.
</p></dd></dl>


<a class="anchor" id="XREFkron"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-kron"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">kron</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>)</code><a class="copiable-link" href="#index-kron"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-kron-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">kron</strong> <code class="def-code-arguments">(<var class="var">A1</var>, <var class="var">A2</var>, &hellip;)</code><a class="copiable-link" href="#index-kron-1"> &para;</a></span></dt>
<dd><p>Form the Kronecker product of two or more matrices.
</p>
<p>This is defined block by block as
</p>
<div class="example">
<pre class="example-preformatted">c = [ a(i,j)*b ]
</pre></div>

<p>For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">kron (1:4, ones (3, 1))
     &rArr;  1  2  3  4
         1  2  3  4
         1  2  3  4
</pre></div></div>

<p>If there are more than two input arguments <var class="var">A1</var>, <var class="var">A2</var>, &hellip;,
<var class="var">An</var> the Kronecker product is computed as
</p>
<div class="example">
<pre class="example-preformatted">kron (kron (<var class="var">A1</var>, <var class="var">A2</var>), ..., <var class="var">An</var>)
</pre></div>

<p>Since the Kronecker product is associative, this is well-defined.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFtensorprod">tensorprod</a>.
</p></dd></dl>


<a class="anchor" id="XREFtensorprod"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-tensorprod"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">tensorprod</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>, <var class="var">dimA</var>, <var class="var">dimB</var>)</code><a class="copiable-link" href="#index-tensorprod"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-tensorprod-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">tensorprod</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>, <var class="var">dim</var>)</code><a class="copiable-link" href="#index-tensorprod-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-tensorprod-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">tensorprod</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>)</code><a class="copiable-link" href="#index-tensorprod-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-tensorprod-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">tensorprod</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>, &quot;all&quot;)</code><a class="copiable-link" href="#index-tensorprod-3"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-tensorprod-4"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">tensorprod</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>, &hellip;, &quot;NumDimensionsA&quot;, <var class="var">value</var>)</code><a class="copiable-link" href="#index-tensorprod-4"> &para;</a></span></dt>
<dd><p>Compute the tensor product between numeric tensors <var class="var">A</var> and <var class="var">B</var>.
</p>
<p>The dimensions of <var class="var">A</var> and <var class="var">B</var> that are contracted are defined by
<var class="var">dimA</var> and <var class="var">dimB</var>, respectively.  <var class="var">dimA</var> and <var class="var">dimB</var> are
scalars or equal length vectors that define the dimensions to match up.
The matched dimensions of <var class="var">A</var> and <var class="var">B</var> must have the same number of
elements.
</p>
<p>When only <var class="var">dim</var> is used, it is equivalent to
<code class="code"><var class="var">dimA</var> = <var class="var">dimB</var> = <var class="var">dim</var></code>.
</p>
<p>When no dimensions are specified, <code class="code"><var class="var">dimA</var> = <var class="var">dimB</var> = []</code>.  This
computes the outer product between <var class="var">A</var> and <var class="var">B</var>.
</p>
<p>Using the <code class="code">&quot;all&quot;</code> option results in the inner product between <var class="var">A</var>
and <var class="var">B</var>.  This requires <code class="code">size (<var class="var">A</var>) == size (<var class="var">B</var>)</code>.
</p>
<p>Use the property-value pair with the property name
<code class="code">&quot;NumDimensionsA&quot;</code> when <var class="var">A</var> has trailing singleton
dimensions that should be transferred to <var class="var">C</var>.  The specified <var class="var">value</var>
should be the total number of dimensions of <var class="var">A</var>.
</p>
<p><small class="sc">MATLAB</small> Compatibility: Octave does not currently support the
<code class="code">&quot;<var class="var">property_name</var>=<var class="var">value</var>&quot;</code> syntax for the
<code class="code">&quot;NumDimensionsA&quot;</code> parameter.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFkron">kron</a>, <a class="ref" href="Utility-Functions.html#XREFdot">dot</a>, <a class="ref" href="Arithmetic-Ops.html#XREFmtimes">mtimes</a>.
</p></dd></dl>


<a class="anchor" id="XREFblkmm"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-blkmm"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">blkmm</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>)</code><a class="copiable-link" href="#index-blkmm"> &para;</a></span></dt>
<dd><p>Compute products of matrix blocks.
</p>
<p>The blocks are given as 2-dimensional subarrays of the arrays <var class="var">A</var>,
<var class="var">B</var>.  The size of <var class="var">A</var> must have the form <code class="code">[m,k,&hellip;]</code> and
size of <var class="var">B</var> must be <code class="code">[k,n,&hellip;]</code>.  The result is then of size
<code class="code">[m,n,&hellip;]</code> and is computed as follows:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for i = 1:prod (size (<var class="var">A</var>)(3:end))
  <var class="var">C</var>(:,:,i) = <var class="var">A</var>(:,:,i) * <var class="var">B</var>(:,:,i)
endfor
</pre></div></div>
</dd></dl>


<a class="anchor" id="XREFsylvester"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-sylvester"><span class="category-def">: </span><span><code class="def-type"><var class="var">X</var> =</code> <strong class="def-name">sylvester</strong> <code class="def-code-arguments">(<var class="var">A</var>, <var class="var">B</var>, <var class="var">C</var>)</code><a class="copiable-link" href="#index-sylvester"> &para;</a></span></dt>
<dd><p>Solve the Sylvester equation.
</p>
<p>The Sylvester equation is defined as:
</p>
<div class="example">
<pre class="example-preformatted">A X + X B = C
</pre></div>

<p>The solution is computed using standard <small class="sc">LAPACK</small> subroutines.
</p>
<p>For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">sylvester ([1, 2; 3, 4], [5, 6; 7, 8], [9, 10; 11, 12])
   &rArr; [ 0.50000, 0.66667; 0.66667, 0.50000 ]
</pre></div></div>
</dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Specialized-Solvers.html">Specialized Solvers</a>, Previous: <a href="Matrix-Factorizations.html">Matrix Factorizations</a>, Up: <a href="Linear-Algebra.html">Linear Algebra</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
